import { Camera } from '@ionic-native/camera/ngx';

export class CameraMock extends Camera {

    images = ['item1.PNG','item2.PNG','item3.PNG'];

    getPicture(options) {
      return new Promise((resolve, reject) => {
        const index = Math.round(Math.random()*this.images.length);
        resolve('assets/imgs/' + this.images[index]);
      })
    }
  }
