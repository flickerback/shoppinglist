import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Item } from '../items';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-itemdetail',
  templateUrl: './itemdetail.page.html',
  styleUrls: ['./itemdetail.page.scss'],
})
export class ItemdetailPage implements OnInit {

  constructor(private route: ActivatedRoute,
  private router: Router,
  private camera: Camera) { }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.loadFromStorage();
    this.item = this.items.find(it => it.id == id);
  }
  items: Item[] = [];
  itemsBought:Item[]=[];
  item: Item;

  //images: string[] = [];
  //imageIndex = -1;

  loadFromStorage() {
    const storage = localStorage.getItem('shoppinglist');
    if(storage != null) {
      this.items = JSON.parse(storage);
    }
  }

  save() {
    this.saveToStorage();
    this.router.navigate(['tabs']);
    
  }

  saveToStorage() {
    localStorage.setItem('shoppinglist', JSON.stringify(this.items));
  }
  saveToStorageBought() {
    localStorage.setItem('boughtlist', JSON.stringify(this.itemsBought));
  }

  getImage() {
    if (this.item.image == "") {
      return 'assets/not-available.jpg';
    } else {
      return this.item.image;
    }
  }

  takePicture() {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetHeight: 1280,
      targetWidth: 1280
    }

    this.camera.getPicture(options).then((imageData) => {
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      //this.item.image=base64Image; ostaje za stavrni plugin
      this.item.image=imageData; //za CameraMock
    });
  }

}
