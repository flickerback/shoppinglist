export class Item {
    id: number;
    title: string;
    store: string;
    quantity: string;
    price: number;
    bought: boolean;
    image:string; 
}