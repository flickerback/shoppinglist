import { Component } from '@angular/core';
import { Item } from '../items';
import { AlertController } from '@ionic/angular';
import { JsonPipe } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page {

  constructor(private alertCtrl: AlertController,
  private router: Router,
  ) {

  }

  ionViewDidEnter() {
    this.loadFromStorage();
    this.loadFromStorageBought();
    this.loadFromStorageBoughtThisSession();
  }

  items: Item[] = [];
  itemsBought:Item[]=[];
  nextId = 1;

  itemsBoughtThisSession:Item[]=[];

  async addItem() {
    const alert = await this.alertCtrl.create(
    {
      header: 'Quick Add',
      inputs: [
        {
          name: 'title',
          type: 'text',
          placeholder: 'Title'
        },
        {
          name: 'store',
          type: 'text',
          placeholder: 'Store'
        },
        {
          name: 'quantity',
          type: 'text',
          placeholder: 'Quantity'
        },
        {
          name: 'price',
          type: 'number',
          placeholder: 'Price'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary'
        },
        {
          text: 'Ok',
          handler: (data) => {
            this.saveItem(data);
          }
        }
      ]
    }
    );
    await alert.present();
  }

  saveItem(data) {
    const item = new Item();
    item.id = this.nextId++;
    item.title = data.title;
    item.store = data.store;
    item.quantity=data.quantity
    item.price=data.price;
    this.items.push(item);
    this.saveToStorage();
  }

  loadFromStorage() {
    const storage = localStorage.getItem('shoppinglist');
    if(storage != null) {
      this.items = JSON.parse(storage);
      this.nextId=this.items.length;
    }
  }
  loadFromStorageBought() {
    const storage = localStorage.getItem('boughtlist');
    if(storage != null) {
      this.itemsBought = JSON.parse(storage);
      this.nextId=this.itemsBought.length;
    }
  }
  loadFromStorageBoughtThisSession() {
    const storage = localStorage.getItem('boughtsessionlist');
    if(storage != null) {
      this.itemsBoughtThisSession = JSON.parse(storage);
      this.nextId=this.itemsBoughtThisSession.length;
    }
  }

  markBought(it: Item) {
    it.bought = true;
    this.itemsBought.push(it);
    this.itemsBoughtThisSession.push(it);
    this.deleteItem(it);
    this.saveToStorage();
    this.saveToStorageBought();
    this.saveToStorageBoughtThisSession();
  }

  saveToStorage() {
    localStorage.setItem('shoppinglist', JSON.stringify(this.items));
  }
  saveToStorageBought() {
    localStorage.setItem('boughtlist', JSON.stringify(this.itemsBought));
  }
  saveToStorageBoughtThisSession() {
    localStorage.setItem('boughtsessionlist', JSON.stringify(this.itemsBoughtThisSession));
  }

  itemClass(it: Item) {
    if(it.bought) {
      return 'bought';
    }
    return '';
  }

  editItem(it: Item) {
    this.router.navigate(['itemdetail/' + it.id]);
  }

  deleteItem(it: Item) {
    const index = this.items.findIndex(x => it.id == x.id);
    this.items.splice(index,1);
    this.saveToStorage();
  }

  doRefresh(event) {
    console.log('Begin async operation');
    this.loadFromStorage();
    this.loadFromStorageBought();
    this.loadFromStorageBoughtThisSession();
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  async showHelp() {
    const alert = await this.alertCtrl.create(
    {
      header: 'Help',
      message: 'This is your shopping list! Click on the plus button to start adding items. Tap on the item to edit it, swipe it to the left to buy it or delete it. <hr> Track info about your current shopping trip by tapping on the info button! <hr> <b>Clear Data </b> clears your shopping trip info, but not your lists, while <b>Clear All</b> clears your lists as well.',
      buttons: ['OK']
    }
    );
    await alert.present();
  }

  getTotalItems(){
    return this.items.length;
  }

  getTotalPrice(){
    var price=0; 
    this.items.forEach(item => {
      var tempPrice=+item.price;
      var tempQty=+item.quantity;

      price+=tempPrice*tempQty;
    });
    return price; 
  }
  getTotalPriceBoughtThisSession(){
    var price=0; 
    this.itemsBoughtThisSession.forEach(item => {
      var tempPrice=+item.price;
      var tempQty=+item.quantity;

      price+=tempPrice*tempQty;
    });
    return price; 
  }

  async showInfo() {
    const alert = await this.alertCtrl.create(
    {
      header: 'Info',
      message: `
      <b>Current shopping session </b> 
      <hr> 
      <div>Items in list: `+this.getTotalItems()+`</div>
      <div>Items bought: `+this.itemsBoughtThisSession.length+`</div>
      <div>Cost of bought items:`+this.getTotalPriceBoughtThisSession()+` </div>
      <div>Cost of unbought items: `+this.getTotalPrice()+`</div>
      `,
      buttons: [
        {
          text: 'Clear All',
          handler: (data) => {
            this.clearAll();
          },
          cssClass: 'secondary'
        },
        {
          text: 'Clear Data',
          handler: (data) => {
            this.clearData();
          },
          cssClass: 'secondary'
        },
        {
          text: 'Ok',
          role: 'cancel',
        },
      ]
    }
    );
    await alert.present();
  }
  clearAll(){
    this.items=[];
    this.itemsBought=[];
    this.itemsBoughtThisSession=[];
    this.saveToStorage();
    this.saveToStorageBought();
    this.saveToStorageBoughtThisSession();
  }

  clearData(){
    this.itemsBoughtThisSession=[];
    this.saveToStorageBoughtThisSession();
  }

}
