import { Component } from '@angular/core';
import { Item } from '../items';
import { AlertController } from '@ionic/angular';
import { JsonPipe } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss'],
})
export class Tab3Page {

  constructor(private alertCtrl: AlertController,
  private router: Router,
  ) {

  }

  ionViewDidEnter() {
    this.loadFromStorage();
    this.loadFromStorageBought();
  }

  itemsBought: Item[] = [];
  nextId = 1;
  items:Item[]=[];

  loadFromStorage() {
    const storage = localStorage.getItem('shoppinglist');
    if(storage != null) {
      this.items = JSON.parse(storage);
      this.nextId=this.items.length;
    }
  }
  loadFromStorageBought() {
    const storage = localStorage.getItem('boughtlist');
    if(storage != null) {
      this.itemsBought = JSON.parse(storage);
      this.nextId=this.itemsBought.length;
    }
  }

  saveToStorage() {
    localStorage.setItem('shoppinglist', JSON.stringify(this.items));
  }
  saveToStorageBought() {
    localStorage.setItem('boughtlist', JSON.stringify(this.itemsBought));
  }

  deleteItem(it: Item) {
    const index = this.itemsBought.findIndex(x => it.id == x.id);
    this.itemsBought.splice(index,1);
    this.saveToStorageBought();
  }
  restoreItem(it:Item){
    it.bought=false;
    this.items.push(it);
    this.deleteItem(it);
    this.saveToStorage();
  }

  doRefresh(event) {
    console.log('Begin async operation');
    this.loadFromStorageBought();
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }
  async showHelp() {
    const alert = await this.alertCtrl.create(
    {
      header: 'Help',
      message: 'This where the items you bought recently go! But, worry not - swipe them to the left to restore them... or delete them forever! ',
      buttons: ['OK']
    }
    );
    await alert.present();
  }
}
